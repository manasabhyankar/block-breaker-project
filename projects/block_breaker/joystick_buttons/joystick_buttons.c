#include "joystick_buttons.h"
#include "gpio.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"

#include <stdbool.h>
#include <stdio.h>

static gpio_s joystick_left;
static gpio_s joystick_right;

static const uint8_t left_button = 10;
static const uint8_t right_button = 11;

extern SemaphoreHandle_t left_button_signal;
extern SemaphoreHandle_t right_button_signal;

static void button_gpio_callback(void) {
  fprintf(stderr, " in isr\n");
  if (LPC_GPIOINT->IO0IntStatF & (1 << left_button)) {
    // send binary semaphore for left button task, clear intr
    xSemaphoreGiveFromISR(left_button_signal, NULL);
    LPC_GPIOINT->IO0IntClr |= (1 << left_button);
  } else if (LPC_GPIOINT->IO0IntStatF & (1 << right_button)) {
    // send binary semaphore for right button task
    xSemaphoreGiveFromISR(right_button_signal, NULL);
    LPC_GPIOINT->IO0IntClr |= (1 << right_button);
  }
}

static void button_intr_init(void) {
  LPC_GPIOINT->IO0IntEnF |= (1 << left_button);
  LPC_GPIOINT->IO0IntEnF |= (1 << right_button);
  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, button_gpio_callback, "button isr");
}

void joystick_buttons__joystick_and_button_init(void) {
  /* P0_15 - Joystick Left, P0_18 - Joystick Right */
  joystick_left = gpio__construct_as_input(GPIO__PORT_0, 15);
  joystick_right = gpio__construct_as_input(GPIO__PORT_0, 18);
  gpio__enable_pull_down_resistors(joystick_left);
  gpio__enable_pull_down_resistors(joystick_right);
  gpio_s left_button_gpio = gpio__construct_as_input(GPIO__PORT_0, left_button);
  gpio_s right_button_gpio = gpio__construct_as_input(GPIO__PORT_0, right_button);
  gpio__enable_pull_down_resistors(left_button_gpio);
  gpio__enable_pull_down_resistors(right_button_gpio);

  left_button_signal = xSemaphoreCreateBinary();
  right_button_signal = xSemaphoreCreateBinary();
  button_intr_init();
}

joystick_position_e joystick_buttons__get_joystick_position(void) {
  joystick_position_e joystick_position = NONE;
  bool joystick_left_value = gpio__get(joystick_left);
  bool joystick_right_value = gpio__get(joystick_right);
  if (!joystick_left_value) {
    joystick_position = JOYSTICK_LEFT;
  } else if (!joystick_right_value) {
    joystick_position = JOYSTICK_RIGHT;
  } else {
    joystick_position = NONE;
  }
  return joystick_position;
}