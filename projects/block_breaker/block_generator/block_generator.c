#include "block_generator.h"
#include "game_state_machine.h"
#include "led_driver.h"
#include <stdio.h>
#include <stdlib.h>

static const uint8_t top_of_game_area = 7;
static const uint8_t block_length = 8;
static const uint8_t block_height = 3;
static const color_code_s color_array[5] = {RED, GREEN, YELLOW, BLUE, PURPLE};
static const uint8_t beginning_of_block = 0;
static const uint8_t end_of_block = 7;

static const uint8_t number_of_blocks_per_row = 64 / block_length;

static const uint8_t block_score = 100;

static void block_generator__generate_block(uint8_t row, uint8_t col, color_code_s color) {
  for (int i = 0; i < block_height; i++) {
    for (int j = 0; j < block_length; j++) {
      if (i == 1) {
        if (j == beginning_of_block || j == end_of_block) {
          led_driver__set_pixel(row + i, col + j, color);
        }
      } else {
        led_driver__set_pixel(row + i, col + j, color);
      }
    }
  }
}

static void block_generator__generate_row_of_blocks(uint8_t row, color_code_s color) {
  uint8_t col = 0;
  uint8_t color_of_block = color;
  while (col < 64) {
    block_generator__generate_block(row, col, color_array[color_of_block]);
    col += block_length;
    /* TODO: read ultrasonic and mod to get random value for block color somehow */
    if (color_of_block == 4) {
      color_of_block = 0;
    } else {
      color_of_block++;
    }
  }
}

void block_generator__generate_blocks_for_level(uint8_t number_of_rows) {
  // red - 1, green - 2, yellow - 3, blue - 4, purple - 5,
  uint8_t row = top_of_game_area + 1;
  uint8_t color = (rand() % 5);
  for (int i = 0; i < number_of_rows; i++) {
    block_generator__generate_row_of_blocks(row, color);
    row += block_height;
    color = rand() % 5;
  }
}

static color_code_s block_generator__return_color_of_block_minus_one(uint8_t row, uint8_t col) {
  color_code_s color = led_driver__get_led_matix_value(row, col);
  color_code_s color_of_new_block = BLACK;
  switch (color) {
  // {RED, GREEN, YELLOW, BLUE, PURPLE};
  case RED:
    color_of_new_block = BLACK;
    break;
  case GREEN:
    color_of_new_block = RED;
    break;
  case YELLOW:
    color_of_new_block = GREEN;
    break;
  case BLUE:
    color_of_new_block = YELLOW;
    break;
  case PURPLE:
    color_of_new_block = BLUE;
    break;
  default:
    color_of_new_block = BLACK;
  }
  return color_of_new_block;
}

void block_generator__delete_block(uint8_t row, uint8_t col) {
  uint8_t start_of_block_col = (col / number_of_blocks_per_row) * number_of_blocks_per_row;
  uint8_t row_offset = 8;
  uint8_t block_row = (row - row_offset) / block_height;
  uint8_t start_of_block_row = (block_row * block_height) + row_offset;
  power_up_e powerup = NO_POWER;

  int value = (rand() % 100);

  if (value < 5) {
    // speed up
    fprintf(stderr, "speed up\n");
    powerup = SPEED_UP;
  } else if (value < 10) {
    // speed down
    fprintf(stderr, "speed down\n");
    powerup = SPEED_DOWN;
  } else if (value < 15) {
    // paddle up
    fprintf(stderr, "paddle up\n");
    powerup = PADDLE_UP;
  } else if (value < 20) {
    // paddle down
    fprintf(stderr, "paddle down\n");
    powerup = PADDLE_DOWN;
  } else {
    // none
    powerup = NONE;
  }

  game_state_machine__use_power_up(powerup);
  game_state_machine__update_score(block_score);
  color_code_s color = block_generator__return_color_of_block_minus_one(start_of_block_row, start_of_block_col);
  block_generator__generate_block(start_of_block_row, start_of_block_col, color);
}