#pragma once

#include <stdint.h>

typedef enum {
  SPEED_UP = 0,
  SPEED_DOWN,
  PADDLE_UP,
  PADDLE_DOWN,
  NO_POWER,
} power_up_e;

void block_generator__generate_blocks_for_level(uint8_t number_of_rows);
void block_generator__delete_block(uint8_t row, uint8_t col);