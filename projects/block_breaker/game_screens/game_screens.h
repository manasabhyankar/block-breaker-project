#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef enum {
  MAINMENU,
  HISCORE,
  OPTIONS,
  GAME,
  GAMEOVER,
} game_screens_e;

void game_screens__set_matrix_to(game_screens_e screen);
void game_screens__display_on_off(bool value);
void game_screens__clear_volume_area(void);
void game_screens__display_score(uint32_t score);
void game_screens__display_gameover_score(uint32_t score);
void game_screens__display_hiscore(uint32_t score, uint8_t row, uint8_t col);