#include "FreeRTOS.h"
#include "delay.h"
#include "game_screens.h"
#include "game_state_machine.h"
#include "joystick_buttons.h"
#include "led_driver.h"
#include "task.h"
#include <stdio.h>

static volatile speed_e speed = NORMAL;
static volatile speed_e delay_speed = NORMAL;

extern SemaphoreHandle_t left_button_signal;
extern SemaphoreHandle_t right_button_signal;

static void left_button_task(void *pv) {
  for (;;) {
    if (xSemaphoreTake(left_button_signal, portMAX_DELAY)) {
      game_state_machine__handle_left_button();
    }
  }
}

static void check_for_speed_up_task(void *pv) {
  for (;;) {
    speed = game_state_machine___check_for_speed_up();
    if (speed == NORMAL) {
      delay_speed = NORMAL;
    } else if (speed == SPEED_UP) {
      delay_speed = SPEED_UP;
    } else if (speed == SPEED_DOWN) {
      delay_speed = SPEED_DOWN;
    }
    vTaskDelay(99);
  }
}

static void right_button_task(void *pv) {
  for (;;) {
    if (xSemaphoreTake(right_button_signal, portMAX_DELAY)) {
      game_state_machine__handle_right_button();
    }
  }
}

static void update_display_task(void *pv) {
  for (;;) {
    led_driver__update_display();
    vTaskDelay(3);
  }
}

static void game_state_machine_task(void *pv) {
  for (;;) {
    game_state_machine__run_game();
    if (speed == SPEED_DOWN) {
      vTaskDelay(36);
    } else if (speed == SPEED_UP) {
      vTaskDelay(24);
    } else {
      vTaskDelay(30);
    }
  }
}

static void joystick_task(void *pv) {
  static joystick_position_e joystick_position = NONE;
  static game_states_e current_state = MAINMENU_s;
  for (;;) {
    joystick_position = joystick_buttons__get_joystick_position();
    current_state = game_state_machine__get_current_state();
    game_state_machine__move_cursor(joystick_position);
    switch (current_state) {
    case MAINMENU_s:
      delay__ms(18);
      break;
    case GAME_s:
      game_state_machine__update_paddle_position(joystick_position);
      break;
    case OPTIONS_s:
      break;
    case HISCORE_s:
      break;
    case GAMEOVER_s:
      break;
    }
    vTaskDelay(18);
  }
}

int main(void) {
  led_driver__init_gpio_pins_for_matrix();
  joystick_buttons__joystick_and_button_init();

  xTaskCreate(joystick_task, "joystick_task", 4096 / sizeof(void *), NULL, PRIORITY_MEDIUM, NULL);
  xTaskCreate(update_display_task, "update_display_task", 4096 / sizeof(void *), NULL, PRIORITY_HIGH, NULL);
  xTaskCreate(game_state_machine_task, "game_state_machine_task", 10000 / sizeof(void *), NULL, PRIORITY_HIGH, NULL);
  xTaskCreate(left_button_task, "left_button_task", 1024 / sizeof(void *), NULL, PRIORITY_HIGH, NULL);
  xTaskCreate(right_button_task, "right_button_task", 1024 / sizeof(void *), NULL, PRIORITY_HIGH, NULL);
  xTaskCreate(check_for_speed_up_task, "check_for_speed_up_task", 1024 / sizeof(void *), NULL, PRIORITY_MEDIUM, NULL);

  puts("Starting RTOS scheduler.\n");
  vTaskStartScheduler();
}