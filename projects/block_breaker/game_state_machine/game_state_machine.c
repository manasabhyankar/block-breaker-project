#include "game_state_machine.h"
#include "block_generator.h"
#include "delay.h"
#include "game_screens.h"
#include "joystick_buttons.h"
#include "led_driver.h"
#include "stdbool.h"
#include "stdlib.h"

#include "delay.h"
#include <stdio.h>

typedef enum {
  TOP_RIGHT = 0,
  TOP_LEFT = 1,
  BOTTOM_LEFT = 2,
  BOTTOM_RIGHT = 3,
} ball_direction_e;

typedef enum {
  NEGATIVE_THREE = -3,
  NEGATIVE_TWO = -2,
  NEGATIVE_ONE = -1,
  POSITIVE_ONE = 1,
  POSITIVE_TWO = 2,
  POSITIVE_THREE = 3,
} ball_slope_e;

typedef enum {
  POSITION_ONE = 0,
  POSITION_TWO = 1,
  POSITION_THREE = 2,
} cursor_position_e;

typedef struct {
  uint8_t ball_row;
  uint8_t ball_col;
  ball_direction_e ball_dir;
} ball_position_s;

static const uint8_t top_of_game_area = 7;
static const uint8_t bottom_of_game_area = 63;
static const uint8_t right_wall_of_game_area = 63;
static const uint8_t left_wall_of_game_area = 0;

static volatile game_states_e current_state = MAINMENU_s;
static volatile ball_slope_e ball_slope = POSITIVE_ONE;
static volatile ball_position_s ball_pos;
static volatile ball_position_s next_pos;
static cursor_position_e current_position = POSITION_ONE;
static volatile uint8_t paddle_length = 0;
static volatile uint8_t paddle_start_col = 0;
static volatile uint8_t paddle_end_col = 0;

static bool game_init = true;
static bool main_menu_init = true;
static bool options_init = false;
static bool hiscores_init = false;
static bool gameover_init = false;

static bool volume_on_off = true;
static power_up_e power_up = NO_POWER;

static volatile uint32_t user_score = 0;
static uint32_t hiscore_arr[5] = {0};

static void game_state_machine__generate_paddle_and_ball(void) {
  const uint8_t initial_paddle_length = 5;
  const uint8_t last_row = 63;
  const uint8_t middle_of_paddle = 2;

  for (int col = 0; col < initial_paddle_length; col++) {
    led_driver__set_pixel(63, 30 + col, WHITE);
    if (col == middle_of_paddle) {
      led_driver__set_pixel(62, 30 + col, CYAN);
      ball_pos.ball_col = 30 + col;
      ball_pos.ball_row = 62;
      ball_pos.ball_dir = TOP_RIGHT;
      next_pos.ball_col = ball_pos.ball_col;
      next_pos.ball_row = ball_pos.ball_row;
      paddle_length = initial_paddle_length;
    }
  }
  paddle_start_col = 30;
  paddle_end_col = paddle_start_col + paddle_length - 1;
}

static void game_state_machine__negate_ball_slope(void) {
  switch (ball_slope) {
  case NEGATIVE_THREE:
    ball_slope = POSITIVE_THREE;
    break;
  case NEGATIVE_TWO:
    ball_slope = POSITIVE_TWO;
    break;
  case NEGATIVE_ONE:
    ball_slope = POSITIVE_ONE;
    break;
  case POSITIVE_ONE:
    ball_slope = NEGATIVE_ONE;
    break;
  case POSITIVE_TWO:
    ball_slope = NEGATIVE_TWO;
    break;
  case POSITIVE_THREE:
    ball_slope = NEGATIVE_THREE;
    break;
  }
}

static void game_state_machine__calculate_next_ball_position(void) {
  switch (ball_pos.ball_dir) {
  case TOP_RIGHT:
    /* column math */
    if (ball_pos.ball_col + ball_slope > right_wall_of_game_area) {
      next_pos.ball_col = right_wall_of_game_area;
    } else {
      next_pos.ball_col += ball_slope;
    }
    /* row math */
    if (ball_pos.ball_row - 1 == top_of_game_area) {
      next_pos.ball_row = top_of_game_area + 1;
    } else {
      next_pos.ball_row = ball_pos.ball_row - 1;
    }
    break;
  case TOP_LEFT:
    if (ball_pos.ball_col + ball_slope < left_wall_of_game_area) {
      next_pos.ball_col = left_wall_of_game_area;
    } else {
      next_pos.ball_col += ball_slope;
    }
    if (ball_pos.ball_row - 1 == top_of_game_area) {
      next_pos.ball_row = top_of_game_area + 1;
    } else {
      next_pos.ball_row = ball_pos.ball_row - 1;
    }
    break;
  case BOTTOM_RIGHT:
    if (ball_pos.ball_col + ball_slope > right_wall_of_game_area) {
      next_pos.ball_col = right_wall_of_game_area;
    } else {
      if (ball_pos.ball_col + ball_slope <= 0) {
        next_pos.ball_col = left_wall_of_game_area;
      } else {
        next_pos.ball_col += ball_slope;
      }
    }
    next_pos.ball_row = ball_pos.ball_row + 1;
    break;
  case BOTTOM_LEFT:
    if (ball_pos.ball_col + ball_slope < left_wall_of_game_area) {
      next_pos.ball_col = left_wall_of_game_area;
    } else {
      next_pos.ball_col += ball_slope;
    }
    next_pos.ball_row = ball_pos.ball_row + 1;
    break;
  }
}

static uint8_t game_state_machine__get_color_of_next_pos(void) {
  uint8_t color = led_driver__get_led_matix_value(next_pos.ball_row, next_pos.ball_col);
  return color;
}

static void game_state_machine__update_ball_pos_with_next_pos(void) {
  ball_pos.ball_col = next_pos.ball_col;
  ball_pos.ball_row = next_pos.ball_row;
}

static bool game_state_machine__check_for_block_collision(uint8_t row, uint8_t col) {
  bool collision_detected = false;
  uint8_t number_of_blocks_per_row = 8;
  uint8_t block_height = 3;

  uint8_t start_of_block_col = (col / number_of_blocks_per_row) * number_of_blocks_per_row;
  uint8_t offset_val = row % block_height;
  uint8_t start_of_block_row = row - (offset_val + 1);

  color_code_s color_of_next_pixel = led_driver__get_led_matix_value(row, col);

  if (color_of_next_pixel == BLACK || color_of_next_pixel == CYAN || color_of_next_pixel == WHITE) {
    collision_detected = false;
  } else {
    collision_detected = true;
  }
  return collision_detected;
}

static bool game_state_machine__check_for_wall_collision(uint8_t row, uint8_t col) {
  bool wall_detected = false;
  if (col == right_wall_of_game_area || col == left_wall_of_game_area) {
    wall_detected = true;
  }
  return wall_detected;
}

static bool game_state_machine__check_for_collision_with_paddle(uint8_t row, uint8_t col) {
  bool paddle_detected = false;
  uint8_t color_of_next_pixel = led_driver__get_led_matix_value(row, col);
  if (color_of_next_pixel == WHITE) {
    paddle_detected = true;
  }
  return paddle_detected;
}

static bool game_state_machine__check_for_collision_with_top(uint8_t row, uint8_t col) {
  bool collision_with_top = false;
  if (row == top_of_game_area + 1) {
    collision_with_top = true;
  }
  return collision_with_top;
}

static bool game_state_machine__check_for_corner_of_blocks() {
  bool ball_in_corner = false;
  uint8_t pixel_above_or_below_ball = BLACK;
  uint8_t pixel_to_side_of_ball = BLACK;
  switch (ball_pos.ball_dir) {
  case TOP_LEFT:
    if (ball_pos.ball_row == top_of_game_area + 1) {
      ball_in_corner = false;
    } else {
      pixel_above_or_below_ball = led_driver__get_led_matix_value(ball_pos.ball_row - 1, ball_pos.ball_col);
      pixel_to_side_of_ball = led_driver__get_led_matix_value(ball_pos.ball_row, ball_pos.ball_col - 1);
      if (pixel_to_side_of_ball != BLACK && pixel_above_or_below_ball != BLACK) {
        /* Ball is in the corner of two blocks */
        ball_in_corner = true;
      }
    }
    break;
  case TOP_RIGHT:
    if (ball_pos.ball_row == top_of_game_area + 1) {
      ball_in_corner = false;
    } else {
      pixel_above_or_below_ball = led_driver__get_led_matix_value(ball_pos.ball_row - 1, ball_pos.ball_col);
      pixel_to_side_of_ball = led_driver__get_led_matix_value(ball_pos.ball_row, ball_pos.ball_col + 1);
      if (pixel_to_side_of_ball != BLACK && pixel_above_or_below_ball != BLACK) {
        ball_in_corner = true;
      }
    }
    break;
  case BOTTOM_RIGHT:
    if (ball_pos.ball_row > 32) {
      ball_in_corner = false;
    } else {
      pixel_above_or_below_ball = led_driver__get_led_matix_value(ball_pos.ball_row + 1, ball_pos.ball_col);
      pixel_to_side_of_ball = led_driver__get_led_matix_value(ball_pos.ball_row, ball_pos.ball_col + 1);
      if (pixel_to_side_of_ball != BLACK && pixel_above_or_below_ball != BLACK) {
        ball_in_corner = true;
      }
    }
    break;
  case BOTTOM_LEFT:
    if (ball_pos.ball_row > 32) {
      ball_in_corner = false;
    } else {
      pixel_above_or_below_ball = led_driver__get_led_matix_value(ball_pos.ball_row + 1, ball_pos.ball_col);
      pixel_to_side_of_ball = led_driver__get_led_matix_value(ball_pos.ball_row, ball_pos.ball_col - 1);
      if (pixel_to_side_of_ball != BLACK && pixel_above_or_below_ball != BLACK) {
        ball_in_corner = true;
      }
    }
    break;
  }
  return ball_in_corner;
}

static void game_state_machine__update_ball_position(void) {
  bool colission_with_block = false;
  bool colission_with_wall = false;
  bool colission_with_paddle = false;
  bool colission_with_top = false;
  uint8_t color_of_next_pixel = BLACK;
  led_driver__set_pixel(ball_pos.ball_row, ball_pos.ball_col, BLACK);
  game_state_machine__calculate_next_ball_position();
  color_of_next_pixel = game_state_machine__get_color_of_next_pos();

  colission_with_block = game_state_machine__check_for_block_collision(next_pos.ball_row, next_pos.ball_col);
  colission_with_wall = game_state_machine__check_for_wall_collision(next_pos.ball_row, next_pos.ball_col);
  colission_with_paddle = game_state_machine__check_for_collision_with_paddle(next_pos.ball_row, next_pos.ball_col);
  colission_with_top = game_state_machine__check_for_collision_with_top(next_pos.ball_row, next_pos.ball_col);

  if (!colission_with_block) {
    /* Did not hit block */
    switch (ball_pos.ball_dir) {
    case TOP_RIGHT:
      if (colission_with_top) {
        ball_pos.ball_dir = BOTTOM_RIGHT;
      } else if (colission_with_wall) {
        ball_pos.ball_dir = TOP_LEFT;
        game_state_machine__negate_ball_slope();
      }
      break;
    case TOP_LEFT:
      if (colission_with_top) {
        ball_pos.ball_dir = BOTTOM_LEFT;
      } else if (colission_with_wall) {
        ball_pos.ball_dir = TOP_RIGHT;
        game_state_machine__negate_ball_slope();
      }
      break;
    case BOTTOM_RIGHT:
      if (colission_with_paddle) {
        next_pos.ball_row--;
        ball_pos.ball_dir = TOP_RIGHT;
        if (colission_with_wall) {
          ball_pos.ball_dir = TOP_LEFT;
          game_state_machine__negate_ball_slope();
        }
      } else if (colission_with_wall) {
        ball_pos.ball_dir = BOTTOM_LEFT;
        game_state_machine__negate_ball_slope();
      } else if (next_pos.ball_row == bottom_of_game_area) {
        // game over!
        current_state = GAMEOVER_s;
      }
      break;
    case BOTTOM_LEFT:
      if (colission_with_paddle) {
        next_pos.ball_row--;
        ball_pos.ball_dir = TOP_LEFT;
        if (colission_with_wall) {
          ball_pos.ball_dir = TOP_RIGHT;
          game_state_machine__negate_ball_slope();
        }
      } else if (colission_with_wall) {
        ball_pos.ball_dir = BOTTOM_RIGHT;
        game_state_machine__negate_ball_slope();
      } else if (next_pos.ball_row == bottom_of_game_area) {
        // game over!
        current_state = GAMEOVER_s;
      }
      break;
    }
  } else {
    /* Block Hit */
    bool ball_in_corner = game_state_machine__check_for_corner_of_blocks(next_pos.ball_row, next_pos.ball_col);
    switch (ball_pos.ball_dir) {
    case TOP_RIGHT:
      if (ball_in_corner) {
        if (ball_slope == POSITIVE_ONE) {
          /* Break all three */
          block_generator__delete_block(next_pos.ball_row, next_pos.ball_col);
        }
        /* Break top and right blocks */
        block_generator__delete_block(ball_pos.ball_row - 1, ball_pos.ball_col);
        block_generator__delete_block(ball_pos.ball_row, ball_pos.ball_col + 1);
        ball_pos.ball_dir = BOTTOM_LEFT;
        game_state_machine__negate_ball_slope();

        next_pos.ball_row = ball_pos.ball_row;
        next_pos.ball_col = ball_pos.ball_col;
      } else {
        /* Delete block hit */
        block_generator__delete_block(next_pos.ball_row, next_pos.ball_col);
        if (next_pos.ball_col % 8 == 0) {
          ball_pos.ball_dir = TOP_LEFT;
          /* TEST */
          next_pos.ball_col = ball_pos.ball_col;
          game_state_machine__negate_ball_slope();
        } else {
          ball_pos.ball_dir = BOTTOM_RIGHT;
        }
        if (next_pos.ball_col == right_wall_of_game_area) {
          ball_pos.ball_dir = BOTTOM_LEFT;
          game_state_machine__negate_ball_slope();
        }
      }
      break;
    case TOP_LEFT:
      if (ball_in_corner) {
        if (ball_slope == NEGATIVE_ONE) {
          block_generator__delete_block(next_pos.ball_row, next_pos.ball_col);
        }
        /* break top and left blocks */
        block_generator__delete_block(ball_pos.ball_row - 1, ball_pos.ball_col);
        block_generator__delete_block(ball_pos.ball_row, ball_pos.ball_col - 1);
        ball_pos.ball_dir = BOTTOM_RIGHT;
        game_state_machine__negate_ball_slope();

        next_pos.ball_row = ball_pos.ball_row;
        next_pos.ball_col = ball_pos.ball_col;
      } else {
        block_generator__delete_block(next_pos.ball_row, next_pos.ball_col);
        if (next_pos.ball_col % 8 == 7) {
          ball_pos.ball_dir = TOP_RIGHT;
          /* TEST */
          next_pos.ball_col = ball_pos.ball_col;
          game_state_machine__negate_ball_slope();
        } else {
          ball_pos.ball_dir = BOTTOM_LEFT;
        }
        if (next_pos.ball_col == left_wall_of_game_area) {
          ball_pos.ball_dir = BOTTOM_RIGHT;
          game_state_machine__negate_ball_slope();
        }
      }
      break;
    case BOTTOM_RIGHT:
      if (ball_in_corner) {
        if (ball_slope == POSITIVE_ONE) {
          block_generator__delete_block(next_pos.ball_row, next_pos.ball_col);
        } else {
          block_generator__delete_block(next_pos.ball_row + 1, next_pos.ball_col);
          block_generator__delete_block(next_pos.ball_row, next_pos.ball_col + 1);
          ball_pos.ball_dir = TOP_LEFT;
          game_state_machine__negate_ball_slope();
        }
        next_pos.ball_row = ball_pos.ball_row;
        next_pos.ball_col = ball_pos.ball_col;
      } else {
        block_generator__delete_block(next_pos.ball_row, next_pos.ball_col);
        if (next_pos.ball_col % 8 == 0) {
          ball_pos.ball_dir = BOTTOM_LEFT;
          /* TEST */
          next_pos.ball_col = ball_pos.ball_col;
          game_state_machine__negate_ball_slope();
        } else {
          ball_pos.ball_dir = TOP_RIGHT;
        }
        if (next_pos.ball_col == right_wall_of_game_area) {
          ball_pos.ball_dir = TOP_LEFT;
          game_state_machine__negate_ball_slope();
        }
      }
      break;
    case BOTTOM_LEFT:
      if (ball_in_corner) {
        if (ball_slope == NEGATIVE_ONE) {
          block_generator__delete_block(next_pos.ball_row, next_pos.ball_col);
        } else {
          block_generator__delete_block(ball_pos.ball_row + 1, ball_pos.ball_col);
          block_generator__delete_block(ball_pos.ball_row, ball_pos.ball_col - 1);
          ball_pos.ball_dir = TOP_RIGHT;
          game_state_machine__negate_ball_slope();
        }
        next_pos.ball_row = ball_pos.ball_row;
        next_pos.ball_col = ball_pos.ball_col;
      } else {
        block_generator__delete_block(next_pos.ball_row, next_pos.ball_col);
        if (next_pos.ball_col % 8 == 7) {
          ball_pos.ball_dir = BOTTOM_RIGHT;
          /* TEST */
          next_pos.ball_col = ball_pos.ball_col;
          game_state_machine__negate_ball_slope();
        } else {
          ball_pos.ball_dir = TOP_LEFT;
        }
        if (next_pos.ball_col == left_wall_of_game_area) {
          ball_pos.ball_dir = TOP_RIGHT;
          game_state_machine__negate_ball_slope();
        }
      }
      break;
    }
  }
  color_code_s next_position = led_driver__get_led_matix_value(next_pos.ball_row, next_pos.ball_col);
  if (next_position != BLACK && next_position != CYAN) {
    // do nothing
  } else {
    game_state_machine__update_ball_pos_with_next_pos();
  }
  led_driver__set_pixel(ball_pos.ball_row, ball_pos.ball_col, CYAN);
}

static void game_state_machine__draw_cursor(uint8_t row, uint8_t col, color_code_s color) {
  led_driver__set_pixel(row, col, color);
  led_driver__set_pixel(row + 1, col, color);
  led_driver__set_pixel(row + 1, col + 1, color);
  led_driver__set_pixel(row + 2, col + 1, color);
  led_driver__set_pixel(row + 2, col + 2, color);
  led_driver__set_pixel(row + 2, col + 1, color);
  led_driver__set_pixel(row + 3, col + 1, color);
  led_driver__set_pixel(row + 3, col, color);
  led_driver__set_pixel(row + 4, col, color);
}

static void game_state_machine__display_cursor(cursor_position_e position, color_code_s color) {
  switch (current_state) {
  case MAINMENU_s:
    switch (position) {
    case POSITION_ONE:
      // point to play
      game_state_machine__draw_cursor(29, 4, color);
      break;
    case POSITION_TWO:
      // point to options
      game_state_machine__draw_cursor(39, 4, color);
      break;
    case POSITION_THREE:
      // point to high scores
      game_state_machine__draw_cursor(49, 4, color);
      break;
    }
    break;
  case GAMEOVER_s:
  case HISCORE_s:
  case GAME_s:
  case OPTIONS_s:
    break;
  }
}

static void game_state_machine__move_cursor_using_joystick_position(joystick_position_e position) {
  static cursor_position_e previous_position = POSITION_THREE;
  switch (position) {
  case JOYSTICK_LEFT:
    if (current_position == POSITION_ONE || current_position == POSITION_TWO) {
      current_position = POSITION_ONE;
    } else {

      current_position = POSITION_TWO;
    }
    break;
  case JOYSTICK_RIGHT:
    if (current_position == POSITION_TWO || current_position == POSITION_THREE) {
      current_position = POSITION_THREE;
    } else {
      current_position = POSITION_TWO;
    }
    break;
  case NONE:
    current_position = current_position;
    break;
  }
  if (current_position == previous_position) {

  } else {
    game_state_machine__display_cursor(previous_position, BLACK);
    game_state_machine__display_cursor(current_position, WHITE);
  }
  previous_position = current_position;
  delay__ms(30);
}

static void game_state_machine__move_paddle(uint8_t starting_col, uint8_t ending_col) {
  led_driver__set_row(63, BLACK);
  paddle_start_col = starting_col;
  paddle_end_col = starting_col + paddle_length - 1;
  for (int j = 0; j < paddle_length; j++) {
    led_driver__set_pixel(bottom_of_game_area, paddle_start_col + j, WHITE);
  }
}

static void game_state_machine__reset_state_inits(game_states_e state) {
  switch (state) {
  case MAINMENU_s:
    game_init = true;
    main_menu_init = false;
    options_init = true;
    hiscores_init = true;
    gameover_init = true;
    break;
  case GAME_s:
    game_init = false;
    main_menu_init = true;
    options_init = true;
    hiscores_init = true;
    gameover_init = true;
    break;
  case OPTIONS_s:
    game_init = true;
    main_menu_init = true;
    options_init = false;
    hiscores_init = true;
    gameover_init = true;
    break;
  case HISCORE_s:
    game_init = true;
    main_menu_init = true;
    options_init = true;
    hiscores_init = false;
    gameover_init = true;
    break;
  case GAMEOVER_s:
    game_init = true;
    main_menu_init = true;
    options_init = true;
    hiscores_init = true;
    gameover_init = false;
    break;
  }
}

static void game_state_machine__check_for_powerup(void) {
  if (power_up == PADDLE_UP) {
    if (paddle_length < 7) {
      paddle_length += 2;
    }
  } else if (power_up == PADDLE_DOWN) {
    if (paddle_length > 3) {
      paddle_length -= 2;
    }
  }
}

static void game_state_machine__sort_hiscores(void) {
  uint32_t temp = 0;
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      if (hiscore_arr[j] < hiscore_arr[j + 1]) {
        temp = hiscore_arr[j];
        hiscore_arr[j] = hiscore_arr[j + 1];
        hiscore_arr[j + 1] = temp;
      }
    }
  }
}

void game_state_machine__update_paddle_position(joystick_position_e position) {
  if (position == JOYSTICK_LEFT) {
    if (paddle_start_col == left_wall_of_game_area) {
      // dont move paddle, will go out of bounds
    } else {
      game_state_machine__move_paddle(paddle_start_col - 1, paddle_end_col - 1);
    }
  } else if (position == JOYSTICK_RIGHT) {
    if (paddle_end_col == right_wall_of_game_area) {
      // dont move paddle, will go out of bounds
    } else {
      game_state_machine__move_paddle(paddle_start_col + 1, paddle_end_col + 1);
    }
  } else if (position == NONE) {
    // NONE - paddle shouldn't move
  }
}

void game_state_machine__run_game(void) {
  switch (current_state) {
  case MAINMENU_s:
    if (main_menu_init) {
      game_screens__set_matrix_to(MAINMENU);
      game_state_machine__display_cursor(POSITION_ONE, WHITE);
      current_position = POSITION_ONE;
      game_state_machine__reset_state_inits(MAINMENU_s);
      user_score = 0;
      uint32_t direction = rand() % 2;
      if (direction == 0) {
        ball_pos.ball_dir = TOP_RIGHT;
        ball_slope = POSITIVE_ONE;
      } else {
        ball_pos.ball_dir = TOP_LEFT;
        ball_slope = NEGATIVE_ONE;
      }
    }
    break;
  case OPTIONS_s:
    if (options_init) {
      game_screens__set_matrix_to(OPTIONS);
      game_screens__display_on_off(volume_on_off);
      game_state_machine__reset_state_inits(OPTIONS_s);
    }
    break;
  case HISCORE_s:
    if (hiscores_init) {
      game_screens__set_matrix_to(HISCORE);
      game_state_machine__reset_state_inits(HISCORE_s);
      game_state_machine__sort_hiscores();
      game_screens__display_hiscore(hiscore_arr[0], 15, 57);
      game_screens__display_hiscore(hiscore_arr[1], 25, 57);
      game_screens__display_hiscore(hiscore_arr[2], 35, 57);
      game_screens__display_hiscore(hiscore_arr[3], 45, 57);
      game_screens__display_hiscore(hiscore_arr[4], 55, 57);
    }

    break;
  case GAME_s:
    if (game_init) {
      game_screens__set_matrix_to(GAME);
      game_state_machine__generate_paddle_and_ball();
      block_generator__generate_blocks_for_level(6);
      game_state_machine__reset_state_inits(GAME_s);
      power_up = NO_POWER;
    }
    game_screens__display_score(user_score);
    game_state_machine__update_ball_position();
    game_state_machine__check_for_powerup();
    break;
  case GAMEOVER_s:
    if (gameover_init) {
      bool score_input = false;
      game_screens__set_matrix_to(GAMEOVER);
      game_state_machine__reset_state_inits(GAMEOVER_s);
      for (int i = 0; i < 5; i++) {
        if (hiscore_arr[i] == 0) {
          hiscore_arr[i] = user_score;
          score_input = true;
          break;
        }
      }
      if (!score_input) {
        if (user_score > hiscore_arr[4]) {
          hiscore_arr[4] = user_score;
        }
      }
      game_screens__display_gameover_score(user_score);
    }
    break;
  }
}

game_states_e game_state_machine__get_current_state(void) { return current_state; }

void game_state_machine__move_cursor(joystick_position_e position) {
  switch (current_state) {
  case MAINMENU_s:
    game_state_machine__move_cursor_using_joystick_position(position);
    break;
  case OPTIONS_s:
    break;
  case HISCORE_s:
    break;
  case GAME_s:
    break;
  case GAMEOVER_s:
    break;
  }
}

void game_state_machine__handle_left_button(void) {
  // left button 'selects'

  switch (current_state) {
  case MAINMENU_s:
    switch (current_position) {
    case POSITION_ONE:
      current_state = GAME_s;
      break;
    case POSITION_TWO:
      current_state = OPTIONS_s;
      break;
    case POSITION_THREE:
      current_state = HISCORE_s;
      break;
    }
    break;
  case OPTIONS_s:
    volume_on_off = !volume_on_off;
    game_screens__clear_volume_area();
    if (volume_on_off) {
      // turn on volume - send to other sj2
    } else {
      // turn off volume - send to other sj2
    }
    game_screens__display_on_off(volume_on_off);
    break;
  case GAMEOVER_s:
    current_state = MAINMENU_s;
    break;
  case GAME_s:
    // no left button function
    break;
  case HISCORE_s:
    // no left button function
    break;
  }
}

void game_state_machine__handle_right_button(void) {
  // right button 'goes back'
  switch (current_state) {
  case OPTIONS_s:
    current_state = MAINMENU_s;
    break;
  case HISCORE_s:
    current_state = MAINMENU_s;
    break;
  case GAMEOVER_s:
    // no right button function
    break;
  case MAINMENU_s:
    // no right button function
    break;
  case GAME_s:
    // no right button function
    break;
  }
}

void game_state_machine__use_power_up(power_up_e powerup) { power_up = powerup; }

speed_e game_state_machine___check_for_speed_up(void) { return power_up; }

void game_state_machine__update_score(uint32_t score) { user_score += score; }