#pragma once

#include "block_generator.h"
#include "joystick_buttons.h"

typedef enum {
  MAINMENU_s = 0,
  OPTIONS_s = 1,
  HISCORE_s = 2,
  GAME_s = 3,
  GAMEOVER_s = 4,
} game_states_e;

typedef enum {
  SLOW = 36,
  NORMAL = 30,
  FAST = 24,
} speed_e;

void game_state_machine__run_game(void);
game_states_e game_state_machine__get_current_state(void);
void game_state_machine__update_paddle_position(joystick_position_e position);
void game_state_machine__move_cursor(joystick_position_e position);
void game_state_machine__handle_left_button(void);
void game_state_machine__handle_right_button(void);
speed_e game_state_machine___check_for_speed_up(void);
void game_state_machine__use_power_up(power_up_e powerup);
void game_state_machine__update_score(uint32_t score);